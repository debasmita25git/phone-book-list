import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'Phone Book';
  firstName: string = 'Coder';
  lastName: string = 'Byte';
  phoneNumber: number = 8885559999;
  phoneNumberArr: any = [];
  sortedPhoneNumberArr: any = [];

  ngOnInit(){

  }

  save(){
    if(this.firstName == null || this.firstName == undefined || this.firstName == ''){
      alert('First name is required!')
    }else if(this.lastName == null || this.lastName == undefined || this.lastName == ''){
      alert('Last name is required!')
    }else if(this.phoneNumber == null || this.phoneNumber == undefined || this.phoneNumber == 0){
      alert('Phone number is required!')
    }else{
      let newArr = {
        firstName: this.firstName,
        lastName: this.lastName,
        phoneNumber: this.phoneNumber
      }
      this.phoneNumberArr.push(newArr);
      this.sortedPhoneNumberArr = this.sortByLastname();
    }
  }

  sortByLastname(){
    this.phoneNumberArr.sort((a: any, b: any)=> {
      if (a.lastName.toLowerCase() < b.lastName.toLowerCase()) {
        return -1;
      }
      if (a.lastName.toLowerCase() > b.lastName.toLowerCase()) {
        return 1;
      }
      return 0;
    });
  }


}
